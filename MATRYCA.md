# Matryca kompatybilności komponentów

## Wersja globalna [1.0.2]

:date: Instalacja TEST: 21.11.2022

:date: Instalacja PROD: 21.11.2022


### Micro integratory

| Komponent                             | Czy wersja się zmieniła | Wersja              | Opis zmian                                                                                                          |
|---------------------------------------|-------------------------|---------------------|--------------------------------------------------------------------------------------------------------------------|
| Order-micro-integrator                        | :heavy_plus_sign:      | 1.0.2               | Dodanie pola dodatkowego identyfikatora orderId do odpowiedzi wszystkich operacji    |
| Pricing-micro-integrator                        | :heavy_minus_sign:      | 1.0.0               | Pierwsza wersja usługi    |
| Inventory-micro-integrator                        | :heavy_minus_sign:      | 1.0.0               | Pierwsza wersja usługi    |
| Customer-micro-integrator                        | :heavy_minus_sign:      | 1.0.0               | Pierwsza wersja usługi    |

## Wersja globalna [1.0.1]

:date: Instalacja TEST: 18.11.2022

:date: Instalacja PROD: 18.11.2022


### Micro integratory

| Komponent                             | Czy wersja się zmieniła | Wersja              | Opis zmian                                                                                                          |
|---------------------------------------|-------------------------|---------------------|--------------------------------------------------------------------------------------------------------------------|
| Order-micro-integrator                        | :heavy_plus_sign:      | 1.0.1               |   Rozbudowanie requestu do linkera o pola "billingFirstName":"APIWSO2" oraz "miApplicationId":"dropshipping" na potrzeby generowania raportów sprzedaży  |
| Pricing-micro-integrator                        | :heavy_minus_sign:      | 1.0.0               | Pierwsza wersja usługi    |
| Inventory-micro-integrator                        | :heavy_minus_sign:      | 1.0.0               | Pierwsza wersja usługi    |
| Customer-micro-integrator                        | :heavy_minus_sign:      | 1.0.0               | Pierwsza wersja usługi    |

## Wersja globalna [1.0.0]

:date: Instalacja TEST: 20.10.2022

:date: Instalacja PROD: 21.10.2022


### Micro integratory

| Komponent                             | Czy wersja się zmieniła | Wersja              | Opis zmian                                                                                                          |
|---------------------------------------|-------------------------|---------------------|--------------------------------------------------------------------------------------------------------------------|
| Order-micro-integrator                        | :heavy_plus_sign:      | 1.0.0               | Pierwsza wersja usługi    |
| Pricing-micro-integrator                        | :heavy_plus_sign:      | 1.0.0               | Pierwsza wersja usługi    |
| Inventory-micro-integrator                        | :heavy_plus_sign:      | 1.0.0               | Pierwsza wersja usługi    |
| Customer-micro-integrator                        | :heavy_plus_sign:      | 1.0.0               | Pierwsza wersja usługi    |




