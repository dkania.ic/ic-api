---
sidebar_position: 1
---

# Frequently Asked Questions

1. Jak skontaktować się z IC

`ToDo`

2. Jak aktywować nowego klienta

3. Jak podpisać umowę
4. Jak rozwiązać problem techniczny
5. Co to jest Dropshiping
6. Dla kogo jest Dropshiping
7. Zwroty i reklamacje towarów
8. Czy identyfikator klienta i identyfikator klienta to ten sam numer?

`NIE. Identyfikator klienta to sześcioznakowy identyfikator przypisany Twojej firmie w systemie Inter Cars ERP. Identyfikator klienta to znacznie dłuższy wewnętrzny identyfikator w API i jest dopasowywany w naszej bazie danych do twojego identyfikatora klienta. Za każdym razem, gdy się zmieniasz swój ClientID w API – i możesz to zrobić w dowolnym momencie – musisz wysłać ten numer na adres icapi@intercars.eu, aby mieć aktualizacja mapowania do systemu ERP.`

9. Co to jest „sku” i jak go zdobyć?
 
`„sku” to identyfikator pozycji w API, unikalny w całym katalogu Inter Cars. Każdy element, każda śruba, opona, wycieraczka itp. może być zidentyfikowane i uporządkowane według tej wartości. Jest alternatywnie nazywany TOWKOD i można go znaleźć na przykład w wygenerowanych plikach CSV ręcznie lub według harmonogramu. Odpowiednią kolumną w pliku CSV jest pole „ID”.`

10. Jaką metodę płatności lub metodę transportu mam wpisać? Za każdym razem dostaję błąd.

`Sposób płatności oraz transport przypisany jest do Twojego konta w systemie ERP i stamtąd jest pobierany. zmuszanie do zastąpienie tych ustawień może spowodować błędy. Aby zapewnić prawidłową realizację zamówienia, nie należy wprowadzać tych wartości ani w ogóle parametry. Twoje konto zostało przetestowane pod kątem tego, zanim zostało przekazane jako właściciel.`

11. W przypadku niektórych produktów otrzymuję błąd w API, chociaż wiem, że są w magazynie.

`Wynika to z faktu, że Sandbox jest środowiskiem testowym i nie obejmuje wszystkich elementów i magazynów skonfigurowanych na produkcji środowisko. Istnieje tylko częściowa reprezentacja, aby środowisko testowe działało szybko i wydajnie podczas testów. Będziesz otrzymuj odpowiedzi z głównych HUBów zamiast z domowego magazynu.`

12. Składam zamówienie, ale nie widzę faktury ani dostawy.

`Sandbox jest środowiskiem testowym i nie symuluje całego cyklu zamówienia. Moment, do którego dotrzesz domyślnie, to umieszczenie zamówienia i pozostaje w statusie „A” oczekując na ręczne potwierdzenie. Tym samym nie przechodzi do kategorii „zrealizowane” lub „zafakturowane” status. Możesz jednak spróbować przeszukać swoje prawdziwe (środowisko produkcyjne) faktury lub dostawy, ponieważ Sandbox oferuje dane historyczne dane dotyczące własnych zakupów.`

13. Podczas wyszukiwania według daty pojawiają się błędy.

`API umożliwia przeszukiwanie całej historii Twoich zakupów, ale tylko w bardzo ograniczonym oknie. Możesz na przykład wyszukać za fakturę sprzed 8 miesięcy, ale tylko w przypadku określenia 7-dniowego przedziału czasowego. Tak więc, aby przeszukać cały miesiąc, trzeba by zapytaj 4 razy, za każdym razem prosząc o kolejny tydzień. Dowody dostawy mają ramkę okienną na 2 dni, ale można je również przeglądać w całości Historia zakupów. `

14. Jak zaimportować listę towarów z pliku CSV do zamówienia API?

`API nie obsługuje importu z plików CSV.`

15. Do kogo mam zadzwonić, gdy mam pytanie?

`Najpierw zawsze staraj się znaleźć odpowiedź w pliku swagger lub definicjach w zakładce API / API PRODUCTS. Jeśli nadal masz problemy, ty możesz wysłać wiadomość e-mail na adres icapi@intercars.eu, podając nazwę aplikacji (z zakładki APLIKACJE) i dokładnie to, co chcesz zrobić, i kroki, które podejmujesz, w tym zrzuty ekranu, łącza i linie kodu oraz komunikaty o błędach, jeśli to możliwe. Ponieważ API jest żywym projektem są stale ulepszane i rozwijane, podczas korzystania z API mogą zajść pewne zmiany, które jeszcze nie nastąpiły zawarte w dokumentacji, którą teraz posiadasz. Problemy techniczne nie są rozwiązywane przez telefon.`

