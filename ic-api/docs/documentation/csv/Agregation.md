---
sidebar_position: 7
---

# Agregacje danych

Dodatkowe agregacje pozwalają na filtrowanie danych wedle życzenie klienta.

1. Język generowania plików > PIC
2. Dostępne segmenty > PIC
3. Dostępne rynki zbytu
4. Waluta zgodnie z konfigurację w ERP lub NAV
5. Agregacja po lokalizacjach 
6. Harmonogramy plików - kiedy mają się generować, np.: 
    - Codziennie harmonogram A
    - Raz w tygodniu harmonogram B1 Poniedziałek B2 Wtorek …….
    - Raz w miesiącu C+dzień miesiąca (np.1 każdego pierwszego dnia w miesiącu)
    - trzeba pamiętać, że plik ProductInformation i WholesalePricing muszą mieć ustawiony ten sam harmonogram
    - w pliku Stock można ustawić harmonogram tak, aby się odświeżał kilka razy na dobę  (co 2h – 3h)  
7. Widoczność indeksu rdzeni w pliku
   - bez widoczności indeksu rdzenia
   - z widocznością indeksu rdzenia 
8. Agregacja po liściach drzewa katalogowego z e-Cat
    - czyli jeśli klient chce same klocki, opony, felgi itp.
9. Agregacja po statusie towaru
10. Agregacja po TecDoc
     - ustawiamy na true/false w zależności czy klient chce tylko produkty które maja numer TecDoc, czy całą ofertę
11.  Możliwość ustawienia by plik był skompresowany (*.zip) lub nie
12.  Możliwość wyboru nazwy generowanego pliku (sufiks z datą dodawaną każdego dnia lub stała nazwa pliku).
      Przykład:
Domyślna nazwa pliku tworzona jest według następującego schematu:
<Typ_pliku_rok_miesiąc_dzień>.csv, np. Stock_2022-05-30.csv, po kompresji plik otrzyma nazwę Stock_2022-05-30.csv.zip
13.  Możliwość ustawienia nadpisywania się pliku, czyli plik będzie miał format Stock.csv i po kompresji Stock.csv.zip
