---
sidebar_position: 1
---

# Wstęp

Pliki CSV mogą być udostępniane na trzy sposoby:
- pod zasobem HTTP,
- na serwerze FTP IC,
- Umieszczane bezpośrednio na serwerze FTP klienta.

Pliki CSV zawierają duże ilości pozycji. Zalecaną formą komunikacji jest, aby klient sam pobierał pliki CSV z zasobu HTTP. 

Funkcjonalność katalogu produktu w przyszłości może zastąpić pobieranie plików CSV.


# Udostępnienie
Pliki udostępniane są w formacie CSV, w którym zastosowano średnik jako separator. Pierwsza linia pliku zawiera nazwy kolumn – nagłówki w rozumieniu RFC 4180 ust. 2.3. Wartości tekstowe podane w polach mogą być ujęte w cudzysłów, np.: „B12 Pro-Kit Sport Suspension Kit (876/943 kg;25/20mm) Subaru BRZ 2.0 06.12-”.

Każdy klient w plikach CSV widzi spersonalizowane pod siebie dane. Pliki CSV klient pobiera ze strony https://data.webapi.intercars.eu/customer po uprzednim otrzymaniu hasła dostępu. 

Pliki CSV udostępniamy tylko na odbiorcy.

# Kategorie
Pliki udostępniane są w **czterech** katalogach:

- ProductInformation – zawiera informacje o produkcie (**domyślny**)
- WholesalePricing – zawiera informacje o cenach netto (**domyślny**)
- Stock – zawiera informacje o dostępności produktów na lokalizacji (**domyślny**)
- Stock_Price – zawiera informacje o cenach i dostępności w danym magazynie (**opcjonalny**) 


# Bezpieczeństwo
Wyżej wymienione dane udostępniane są przez serwer z użyciem protokołu HTTPS. Autentykacja następuje z wykorzystaniem mechanizmów BasicAuth zgodnie z dokumentami RFC 7617 oraz RFC 7235. Pliki możliwe są do pobrania przez użytkowników końcowych manualnie za pomocą przegląrarki internetowej lub innego narzędzia komunikującego się z użyciem protokołu HTTP oraz automatycznie za pomocą wbudowanej integracji z naszym systemem propagującym ww. dane - z użyciem tego samego protokołu.
Do manualnej edycji pobranego pliku zaleca się użycie oprogramowania Notepad++, dostępnego do pobrania pod adresem <https://notepad-plus-plus.org/downloads/>. Korzystając z Excela firmy Microsoft, należy mieć na uwadze jego ograniczoną pojemność do nieco ponad 1 mln wierszy. Stosowanie wersji MS Excel poniżej 2007 nie jest zalecane. 

