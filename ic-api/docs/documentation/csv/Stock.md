---
sidebar_position: 4
---

# Stock

Zestawienie produktów zawierające informacje o stanach magazynowych
- Pliki te dostępne są w katalogu Stock
- Pliki generowane są około 03:00 – 04:20 AM czasu polskiego
- Pliki zawierają dane pogrupowane w następujących kolumnach:

| Lp. | Nazwa pola   | Typ danych | Opis                                           |
|-----|--------------|------------|------------------------------------------------|
| 1.  | TOW_KOD      | TEKST      | Unikalny identyfikator produktu (towkod SAFO)  |
| 2.  | IC_INDEX     | TEKST      | Unikalny identyfikator produktu (Indeks SAFO)  |
| 3.  | TEC_DOC      | TEKST      | Identyfikator produktu z Tec Doc (ArtNr)       |
| 4.  | TEC_DOC_PROD | LICZBA     | Identyfikator producenta z Tec Doc             |
| 5.  | WAREHOUSE    | TEKST      | Magazyn lub Filia                              |
| 6.  | AVAILABILITY | LICZBA     | Liczba dostępnych produktów                    |