---
sidebar_position: 3
---

# WholesalePricing

Zestawienie produktów zawierające informacje o cenach netto dla klienta
- Pliki te dostępne są w katalogu WholesalePricing
- Pliki generowane są około 04:30 – 05:30 AM czasu polskiego
- Pliki zawierają dane pogrupowane w następujących kolumnach:

| **Lp.** | **Nazwa pola**      | **Typ danych** | **Opis **                                            |
|---------|---------------------|----------------|------------------------------------------------------|
| 1.      | TOW_KOD             | TEKST          | Unikalny identyfikator produktu (towkod SAFO)        |
| 2.      | IC_INDEX            | TEKST          | Unikalny identyfikator produktu (Indeks SAFO)        |
| 3.      | TEC_DOC             | TEKST          | Identyfikator produktu z Tec Doc (ArtNr)             |
| 4.      | TEC_DOC_PROD        | LICZBA         | Identyfikator producenta z Tec Doc                   |
| 5.      | WHOLESALE_NET_PRICE | LICZBA         | Cena hurtowa                                         |
| 6.      | CORE_PRICE          | LICZBA         | Cena części podlegającej regeneracji                 |
| 7.      | SUM_PRICE           | LICZBA         | Suma ceny hurtowej i części podlegającej regeneracji |