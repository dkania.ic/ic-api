---
sidebar_position: 2
---

# ProductInformation

Zestawienie produktów zawierające opisy o produkcie
- Pliki są dostępne w katalogu ProductInformation
- Pliki generowane są raz dziennie około godziny 04:00-05:30 AM czasu polskiego
- Pliki zawierają dane pogrupowane w następujących kolumnach:

| Lp. | Nazwa pola        | Typ danych | Opis                                          |
|-----|-------------------|------------|-----------------------------------------------|
| 1   | TOW_KOD           | TEKST      | Unikalny identyfikator produktu (towkod SAFO) |
| 2   | IC_INDEX          | TEKST      | Unikalny identyfikator produktu (Indeks SAFO) |
| 3   | TEC_DOC           | TEKST      | Identyfikator produktu z Tec Doc (ArtNr)      |
| 4   | TEC_DOC_PROD      | LICZBA     | Identyfikator producenta z Tec Doc            |
| 5   | ARTICLE_NUMBER    | TEKST      | Identyfikator z katalogu części w SAFO        |
| 6   | MANUFACTURER      | TEKST      | Nazwa producenta z SAFO                       |
| 7   | SHORT_DESCRIPTION | TEKST      | Krótki opis produktu z SAFO                   |
| 8   | DESCRIPTION       | TEKST      | Opis rozszerzony z SAFO                       |
| 9   | BARCODES          | TEKST      | Kody EAN                                      |
| 10  | PACKAGE_WEIGHT    | LICZBA     | Waga produktu                                 |
| 11  | PACKAGE_LENGTH    | LICZBA     | Długość produktu                              |
| 12  | PACKAGE_WIDTH     | LICZBA     | Szerokość produktu                            |
| 13  | PACKAGE_HEIGHT    | LICZBA     | Wysokość produktu                             |
| 14  | CUSTOM_CODE       | LICZBA     | Kod celny                                     |

![dropshippingImage1](../../img/productInformation.png)

