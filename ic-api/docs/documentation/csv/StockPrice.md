---
sidebar_position: 5
---

# StockPrice

Zestawienie produktów zawierające informacje o stanach magazynowych i cenach dla klienta
- Pliki te dostępne są w katalogu Stock_price
- Pliki generowane są około 03:00 – 04:20 AM czasu polskiego
- Pliki zawierają dane pogrupowane w następujących kolumnach:

| **Lp.** | **Nazwa pola**  | **Typ danych** | **Opis **                                             |
|---------|-----------------|----------------|-------------------------------------------------------|
| 1.      | TOW_KOD         | TEKST          | Unikalny identyfikator produktu (towkod SAFO)         |
| 2.      | WAREHOUSE       | TEKST          | Magazyn lub Filia                                     |
| 3.      | AVAILABILITY    | LICZBA         | Liczba dostępnych produktów                           |
| 4.      | WHOLESALE_PRICE | LICZBA         | Cena hurtowa                                          |
| 5.      | CORE_PRICE      | LICZBA         | Cena części podlegającej regeneracji                  |
| 6.      | SUM_PRICE       | LICZBA         | Suma ceny hurtowej i części podlegającej regeneracji  |
| 7.      | RETAIL_PRICE    | LICZBA         | Detal netto                                           |