---
sidebar_position: 4
---

# DevPortal

Opis przeprowadzenia testu z użyciem aplikacji DevPortal:

1.  Wchodzimy na stronę

    <https://cp.webapi.intercars.eu/devportal/apis>

2.  W oknie logowania

    a.  podajemy login i hasło

    ![image3-1.png](../../img/image3-1.png)

    b.  Prosimy nie wybierać Tworzenia konta - funkcjonalność ta jest tymczasowo niedostępna

    c.  Prosimy nie wybierać Przypomnienia Hasła - funkcjonalność ta jest tymczasowo niedostępna - jeśli zapomnimy hasło, prosimy skontaktować się z IC celem wygenerowania nowego hasła.

3.  Po zalogowaniu wyświetla się ekran główny z listą dostępnych
    aplikacji:

    1.  Zakładka API pozwalająca na wykonanie podstawowych operacji z API

    2.  Zakładka Applications pozwalająca na wygenerowanie aplikacji grupujących API

    3. Informacja o zalogowanym użytkowniku

    4. Ekran główny wyświetlający szczegóły wybranej opcji 1 lub 2

    ![image3-2.png](../../img/image3-2.png)

4.  Przechodzimy do szczegółów wybierając API 1 oraz IC 4 (w zależności od uprawnień zalogowany użytkownik może widzieć więcej API np. IC, Dropshiping, Product). Po wybraniu szczegółów dostajemy ekran z:

    a.  Informacjami ogólnymi

    b.  Subskrypcją -- możliwość podglądu kluczy

    c.  Testem -- możliwość testowania aplikacji

    d.  Komentarzami -- możliwość komentowania

    e.  Dokumentacją -- szczegółowy opis pojawi się w kolejnych
        wersjach.

    ![image3-3.png](../../img/image3-3.png)

5.  Zakładka Subscription:

    a.  Pozwala dla wybranej aplikacji na podejrzenie Key and Secret dla
        środowisk Sandbox oraz Produkcji -- wartości konsoli DevPortal
        dla wybranych środowisk powinny być identyczne z ClientID i
        ClientSecret przekazanymi przez IC

    b.  Przycisk Generate Access Token pozwala na wygenerowanie tokenu
        wykorzystywanego przy testowaniu

    ![image3-4.png](../../img/image3-4.png)

6.  Zakładka Try Out Pozwala na przetestowanie usługi:

    a.  Wybieramy środowisko

    b.  Generujemy Token -- możemy go podejrzeć

    ![image3-5.png](../../img/image3-5.png)

    c.  Wybieramy operację którą chcemy przetestować, czytamy dokumentację

    ![image3-6.png](../../img/image3-6.png)

    d.  Wybieramy przycisk „**Try it out**" oraz Podajemy parametry i uruchamiamy „**Execute**"

    ![image3-7.png](../../img/image3-7.png)

    e.  Widzimy dokładny request wysłany do API oraz odpowiedź serwera

    ![image3-8.png](../../img/image3-8.png)

    f.  Z poziomu zakładki **Try it out** możemy wywołać dowolną operację na API IC łącznie ze złożeniem zamówienia. Wykonanie takiej operacji jest równoważne ze złożeniem zamówienia które będzie od razu procesowane przez magazyn IC.


7.  Zakładka Application pozwala na sprawdzenie planów przypisanych do
    aplikacji oraz ustawień bezpieczeństwa podobnie jak w opcji powyżej.

    ![image3-9.png](../../img/image3-9.png)

8.  Po wybraniu szczegółów aplikacji wyświetlają się szczegóły, w których możemy:

    a.  Podejrzeć ustawienia dla środowiska Produkcyjnego

    b.  Podejrzeć ustawienia dla środowiska Sandbox

    c.  Podejrzeć ustawienia dla subskrybowanych API

    ![image3-10.png](../../img/image3-10.png)

    **UWAGA !!!**

    Prosimy nie zmieniać ustawień dla Application i Subscription. Dodanie nowej konfiguracji nie będzie obsługiwane przez IC. Prosimy bazować na tych które są stworzone i traktować te dane jedynie do podglądu.

9.  Możemy też zmienić wygenerowane hasło na swoje własne wybierając login użytkownika oraz opcję „**Change Password**"

    ![image3-11.png](../../img/image3-11.png)
