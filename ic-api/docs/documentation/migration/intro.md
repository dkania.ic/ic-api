---
sidebar_position: 1
---

# Wstęp

**Zmiany**

W związku ze zmianą serwerów API Inter Cars, pojawia się konieczność przekonfigurowania połączenia Państwa oprogramowania zakupowego/warsztatowego.

Zmiana ta ma na celu zoptymalizowanie narzędzia API Inter Cars a tym samym zwiększenie jego wydajności i szybkości działania.

Modyfikacje po stronie klientów są bardzo proste do przeprowadzenia. 

**Z punktu widzenia:**

- **programistycznego: wszystkie interfejsy (usługi, operacje, parametry) <u>nie ulegną</u> zmianie.**
- **administracyjnego:**

   - **Zmieni się adres URL serwera API**
   - **Zmieni się ClientId i ClientSecret (dla środowiska Prod i Sandbox)**
   - **Zmienia się również URL, login, hasło oraz funkcjonalność DevPortal.**

Wszystkie stare konta zostaną zarejestrowane automatycznie w DevPortal przez Zespół API Inter Cars. Nowe adresy URL oraz ClientID i ClientSecret dla wszystkich Klientów zostaną przekazane indywidualnie.

Prawidłowy przebieg procesu migracji:
-   Otrzymanie nowych danych konfiguracyjnych
-   Weryfikacja poprawności połączenia i pobrania danych za pomocą narzędzi – Postman, Curl, DevPortal (krok opcjonalny)
-   Rekonfiguracja docelowych aplikacji zakupowych/warsztatowych na podstawie instrukcji od poszczególnych dostawców aplikacji sprzedażowych
-   Weryfikacja poprawności działania z poziomu aplikacji zakupowych/warsztatowych.

Rekonfiguracja API  Inter Cars jest bardzo mocno uzależniona od Państwa aplikacji zakupowych/warsztatowych i w tym przypadku zalecamy kontakt z dostawcą oprogramowania.
<br />

Poniższy opis przedstawia sposób weryfikacji Państwa nowych ustawień z wykorzystaniem narzędzi ogólnodostępnych.

Jest to krok opcjonalny i docelowa weryfikacja powinna być przeprowadzona z Państwa docelowych aplikacji sprzedażowych.

W razie pytań, nasze Wsparcie Zespołu API jest dostępne pod adresem: [icapi@intercars.eu]( <mailto:icapi@intercars.eu> "icapi@intercars.eu").
