---
sidebar_position: 2
---

# Linia komend

Opis przeprowadzenia testu z Linii komend Linux i Windows

1.  Generujemy token zgodnie z wzorcem

    a.  Wchodzimy na stronę <https://www.base64encode.org/>

    b.  Wybieramy opcje Encode i wprowadzamy zapis ClientID:ClientSecret

    c.  Wynik przeklejamy do przykładu poniżej w miejsce
        **<TOKENBASE64\>**

    > Najlepiej używając notepad lub innego edytora tekstu. Dalsze działania uzależnione są od używanego systemu operacyjnego.

    Wersja Windows

    `curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -H "Authorization: Basic <TOKENBASE64>" https://is.webapi.intercars.eu/oauth2/token -d "grant_type=client_credentials&scope=allinone" `


    Wersja Linux

    `curl --location --request POST 'https://is.webapi.intercars.eu/oauth2/token?grant_type=client_credentials&scope=allinone' \
    --header 'Content-Type: application/x-www-form-urlencoded' \ 
    --header 'Authorization: Basic <TOKENBASE64>'`

    d.  W windows uruchamiamy wiersz poleceń CMD lub Shell na linux

    e.  Wklejamy powyższą zawartość i uruchamiamy naciskając enter

    f.  W odpowiedzi dostajemy wynik i zapisujemy zawartość pola access_token


2.  Wywołujemy usługę biznesową

    a.  Używając edytora tekstu kopiujemy access_token do żądania biznesowego jak poniżej:

    Wersja Windows

    ` curl -X GET -H "Accept: application/json" -H "Authorization: Bearer <TOKENOAUTH>" "https://api.webapi.intercars.eu/ic/inventory/stock?sku=ADDFFF&location=KOM" `

    Wersja Linux

    ` curl --location --request GET 'https://api.webapi.intercars.eu/ic/inventory/stock?sku=ADDFFF&location=KOM' \
    --header 'Accept: application/json' \
    --header 'Authorization: Bearer <TOKENOAUTH>' `


    b.  W Windows uruchamiamy wiersz poleceń CMD lub Shell na linux

    c.  Wklejamy powyższą zawartość i uruchamiamy naciskając enter

    d.  W odpowiedzi dostajemy wynik biznesowy -- np. dostępność towaru na lokalizacji.

    Poniżej przykład pełnej komunikacji

    -   W pkt 2 wklejamy zakodowany clientID:clientSecret

    -   W pkt 3 z odpowiedzi zapamiętujemy pole access_token

    -   W pkt 4 przygotujemy docelowe żądanie biznesowe (np. zapytanie o stan)

    -   W pkt 5 wklejamy zawartość wygenerowanego access_token

    -   W odpowiedzi uzyskujemy wynik jak w pkt 6

    ![image2-1.png](../../img/image2-1.png)
