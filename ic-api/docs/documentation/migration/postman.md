---
sidebar_position: 2
---

# Postman

**Opis przeprowadzenia testu z użyciem programu Postman**

1.  Importujemy pliki z Postman

    -   [IC.postman_collection.json](../../postman/IC.postman_collection.json)
    -   [PROD IC.postman_environment.json](../../postman/PROD_IC.postman_environment.json)
    -   [PROD IBM.postman_environment.json](../../postman/PROD_IBM.postman_environment.json)

    Poniżej opis jak zaimportować pliki do programu [postman][Rr]   



    ![Diagram](../../img/image1.png)[🔍Pełny rozmiar w nowej karcie](../../img/image1.png)

2.  Konfigurujemy ustawienia dla IBM:

    a.  Wybieramy zakładkę Environments

    b.  Następnie PROD IBM

    c.  Po prawej wypełniamy ClientID i ClientSecret

    d.  Zapisujemy zmiany (Ctrl-S) -- po zapisie zniknie czerwona kropka
        obok napisu PROD IBM

    ![image2.png](../../img/image2.png)[🔍Pełny rozmiar w nowej karcie](../../img/image2.png)

3.  Uwierzytelniamy wywołanie na IBM:

    a.  Wybieramy zakładkę Collections

    b.  Następnie rozwijamy projekt IC i wybieramy operacje Authorize

    c.  Pojawia się nowa zakładka (nie musimy nic zmieniać)

    d.  Po prawej wybieramy środowisko PROD IBM

    e.  Wywołujemy operacje Send

    f.  W odpowiedzi w oknie głównym wyświetla się komunikat z zawartością tokenu

        Token wykorzystywany będzie przy kolejnych wywołaniach

    g.  Na dole w zakładce Console możemy zobaczyć dokładną zawartość żądania jaką wygenerował i wysłał Postman:

    ![image3.png](../../img/image3.png)[🔍Pełny rozmiar w nowej karcie](../../img/image3.png)

4.  Wywołujemy operację biznesową:

    a.  W zakładce Collections

    b.  Wybieramy projekt IC i np. operację Inventory (domyślnie odpytujemy o towar ADDFFF)

    c.  Wywołujemy operacje Send (warto się upewnić, że wybrane jest środowisko PROD IBM)

    d.  W odpowiedzi dostajemy informacje o stanie produktu

    e.  W logu możemy podejrzeć szczegóły komunikacji HTTP

    ![image4.png](../../img/image4.png)[🔍Pełny rozmiar w nowej karcie](../../img/image4.png)

    **API Poprawnie działa na starych ustawieniach**

5.  Przepisujemy konfigurację na nowe serwery IC API:

    a.  Wybieramy zakładkę Enviroments

    b.  Następnie PROD IC

    c.  Otwiera się nowa zakładka w oknie głównym

    d.  Po prawej wypełniamy ClientID i ClientSecret (nowe wartości
        przekazane przez IC)

    e.  Nowe adresy serwerów IC wprowadzone są już w pliku
        konfiguracyjnym w polach baseUrl i tokenUrl

    f.  Zapisujemy zmiany (Ctrl-S) -- po zapisie zniknie czerwona kropka
        obok napisu PROD IC (pkt3)

    ![image5.png](../../img/image5.png)[🔍Pełny rozmiar w nowej karcie](../../img/image5.png)

6.  Authentykujemy wywołanie na nowych serwerach IC:

    a.  Wybieramy zakładkę Collections

    b.  Następnie rozwijamy projekt IC i wybieramy operacje Authorize

    c.  Pojawia się nowa zakładka (nie musimy nic zmieniać)

    d.  Po prawej wybieramy środowisko **PROD IC**

    e.  Wywołujemy operacje Send

    f.  W odpowiedzi w oknie głównym wyświetla się komunikat z zawartością tokenu

    ![image6.png](../../img/image6.png)[🔍Pełny rozmiar w nowej karcie](../../img/image6.png)

7.  Wywołujemy operację biznesową:

    a.  W zakładce Collections

    b.  Wybieramy projekt IC i np. operację Inventory (domyślnie odpytujemy o towar ADDFFF)

    c.  Wywołujemy operacje Send (warto się upewnić że wybrane jest środowisko **PROD IC**)
    
    d.  W odpowiedzi dostajemy informacje o stanie produktu 
        Można porównać ją z odpowiedzią uzyskaną podczas odpytania IBM
        
    e.  W logu możemy podejrzeć szczegóły komunikacji http

    f.  Operację możemy powtórzyć dla dowolnych innych operacji z projektu Postman

    ![image7.png](../../img/image7.png)[🔍Pełny rozmiar w nowej karcie](../../img/image7.png)

    [Rr]: <https://learning.postman.com/docs/getting-started/importing-and-exporting-data/>