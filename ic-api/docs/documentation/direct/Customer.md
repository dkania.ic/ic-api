---
sidebar_position: 2
---

# Customer

## Customer finances

Ta operacja zwraca szczegółowe dane finansowe klienta.
Operacja jest dedykowana do sprawdzenia, czy istnieje możliwość składania zamówień w sklepie internetowym.
Zwraca zestaw danych z systemu ERP, takich jak:
- dostępne limity
- dostępny kredyt
- dozwolone zamawianie

Z usługi należy korzystać minimum raz dziennie, a Operator Dropshipping musi regularnie sprawdzać odpowiedź przed rozpoczęciem wysyłania zamówień.
Zakres danych zwracanych przez serwis jest wystarczający do podjęcia działań zmniejszających ryzyko zablokowania przez IC realizacji zleceń.

**Endpoint:**

`GET /customer/finances`

## Customer 

Ta operacja zwraca szczegółowe dane adresowe i logistyczne klienta.

**Endpoint:**

`GET /customer`
