---
sidebar_position: 7
---

## Wyszukanie szczegółów wybranej faktury

Ta operacja pobiera szczegółowe dane dotyczące pojedynczej faktury. Unikalny identyfikator (invoiceID) jest przekazywany na końcu URI wywołania. Na przykład jeśli chcemy uzyskać szczegółowe informacje o swojej fakturze PL00000001, musimy wywołać żądanie jako /invoice/PL00000001 . W odpowiedzi otrzymamy szczegóły faktury zawierające referencje do zamówień i dostaw, umożliwiające dopasowanie dokumentu do zamówień / zapotrzebowań. Jeżeli identyfkator faktury jest technicznym id to na końcu URI wywołania należy dodać opcjonalny parametr zapytania techId z wartością true

**Endpoint:**

`GET /invoice/invoiceID?techId=true`

## Przeglądanie nagłówków faktur z wybranego zakresu czasu

Ta operacja wyszukuje faktury związane z Państwa zapotrzebowaniami w siedmiodniowym oknie czasowym w dowolnym okresie z przeszłości. Na przykład możesz ograniczyć przeszukiwanie do ostatnich siedmiu dni sierpnia, podając: " /invoice?issueDateFrom=2021-08-26&issueDateTo=2021-08-31 " . Dodatkowo, ta metoda obsługuje kontrolę stronicowania (pola limitów i offsetów), które pozwalają kontrolować rozmiar strony zwróconych zamówień. Zauważ, że jeśli nie zdefiniujesz kontroli paginacji, zostanie przyjęta wartość domyślna.

**Endpoint:**

`GET /invoice/`


