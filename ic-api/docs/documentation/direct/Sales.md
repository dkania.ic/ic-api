---
sidebar_position: 5
---

# Sales


## Składanie zamówienia

Ta operacja służy do tworzenia zapotrzebowania na konkretne towary. W zależności od konfiguracji, zamówienie może być procesowane z potwierdzeniem automatycznym, albo oczekiwać na Twoje ręczne potwierdzenie lub anulowanie. Informacje, które będą wymagane do wysłania zamówienia to: SKU przedmiotu (TOWKOD), cena netto klienta i wymagana ilość. Dodatkowo możesz zapisać w zamówieniu swój własny numer zamówienia dodając parametr "customNumber" jak również zamieścić komentarz dotyczący np. szczegółów dostawy (przykłądowo: "comments":"Proszę dzwonić przy szlabanie"). Jeśli przesyłanie zamówienia zakończy się powodzeniem, w odpowiedzi zwracany jest numer/numery zamówienia z unikalnym identyfikatorem zapotrzebowania (requisition ID). Ten identyfikator będzie wymagany w przypadku innych odpytań związanych z zapotrzebowaniem. 

-------------
Dla kont specjalnych typu Master, należy w treści wysyłanego zamówienia lub URL pobieranego statusu dodatkowo podać parametr "shipTo" identyfikujący pożądanego odbiorcę. Identyfikatory przekazywane będą przez Inter Cars podczas integracji.

**Endpoint:**

`POST /sales/requisition`


## Pobieranie listy z wybranego przedziału czasowego

Ta operacja wyszukuje zamówienia związane z Twoimi zapotrzebowaniami (requisitionID) w maksymalnie siedmiodniowym oknie czasowym w dowolnym okresie z przeszłości. Na przykład możesz ograniczyć przeszukiwanie do ostatnich trzech dni ubiegłego roku, podając: " /sales/requisition?creationDateFrom=2021-12-29&creationDateTo=2021-12-31 ". Dodatkowo, ta metoda obsługuje kontrolę stronicowania (pola limitów i offsetów), które pozwalają kontrolować rozmiar strony zwróconych zamówień. Zauważ, że jeśli nie zdefiniujesz kontroli paginacji, zostanie przyjęta wartość domyślna.

**Endpoint:**

`GET /sales/requisition`


## Pobieranie szczegółów zamówienia po identyfikatorze zapotrzebowania (requisitionID)

To operacja pobiera szczegóły potwierdzonego, jak i niepotwierdzonego zamówienia związanego ze wskazanym zapotrzebowaniem. Unikalny identyfikator (requisitionID) jest przekazywany na końcu identyfikatora URI wywołania. Na przykład, jeśli chcesz uzyskać szczegółowe informacje na temat zapotrzebowania IC10000001, musisz wywołać żądanie jako  " /sales/requisition/IC10000001 ". W odpowiedzi otrzymasz kolekcję zamówień, która przedstawi aktualny stan realizacji.

**Endpoint:**

`GET /sales/requisition/{requisitionID}`


## Pobieranie szczegółów zamówienia po identyfikatorze ERP

To operacja pobiera szczegółowe dane o pojedynczym zamówieniu z Twojego żądania. Unikalny identyfikator (orderID) jest przekazywany na końcu identyfikatora URI wywołania. Na przykład, jeśli chcesz uzyskać szczegółowe informacje o zamówieniu 1100130601, musisz wywołać żądanie jako /sales/order/1100130601. W odpowiedzi otrzymasz szczegółowe informacje o pojedynczym zamówieniu z Twojego całościowego zamówienia. Ta metoda jest przydatna w procesie fakturowania i wysyłki oraz pozwala sprawdzić, dla którego zamówienia przygotowano te dokumenty.

**Endpoint:**

`GET /sales/order/oderID`




