---
sidebar_position: 3
---

# Inventory - Stock

Ta operacja pobiera szczegóły dostępności magazynowej wybranych w zapytaniu produktów dostępnych do zakupu. Jeśli lokalizacja magazynu Inter Cars jest określona w wywołaniu operacji, stan jest prezentowany tylko dla tego magazynu. W przeciwnym razie stan jest zwracany dla wszystkich magazynów ze skonfigurowanej ścieżki logistycznej klienta.

Usługa Inventory/Stock ma na celu określenie dostępności listy produktów według wprowadzonych parametrów.
W parametrach wejściowych należy określić:
- kod magazynu (parametr opcjonalny)
- identyfikator SKU, tzw TOWKOD

W rezultacie usługa zwraca dla każdego artykułu i magazynu liczbę większą lub równą „0” (zero) wraz ze statycznym terminem dostawy. Usługa obsługuje jednocześnie listę do 30 pozycji.
Serwis nie interpretuje wyniku – logikę należy zaimplementować po stronie platformy klienta.
Wyślij zapytanie magazynowe za pomocą GET /inventory/stock, podając sku (pole TOWKOD / INDEX / ID z pliku InterCars CSV)

**Endpoint:** 

`GET /inventory/stock`


-------------
Dla kont specjalnych typu Master, należy w treści wysyłanego zapytania dodatkowo podać parametr "shipTo" definiujący pożądanego odbiorcę. Identyfikatory przekazywane będą przez Inter Cars podczas integracji.
