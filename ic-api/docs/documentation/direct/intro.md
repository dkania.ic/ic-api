---
sidebar_position: 1
---

# Wstęp

Model IC API Direct pozwala na sprzedaż towaru i dostarczenie do magazynu partnera. Wśród operacji i funkcjonalności wspomagających zakup/sprzedaż wyróżniamy między innymi:

- sprawdzenie dostępności produktów (jedno żądanie do 100 pozycji), z możliwością wskazania magazynów ze ścieżki logistycznej,
- pobranie aktualnych cen produktów (jedno żądanie dla do 20 pozycji),
- złożenie zamówienia z aktualnymi cenami,
- dodanie własnego numeru zamówienia,
- dodanie komentarza do zamówienia,
- sterowanie konfigurowanymi odbiorcami płatnika w obrębie zamówień i odpytań o stan magazynowy (konta specjalne typu Master),
- otrzymanie identyfikatora zamówienia: requisition ID,
- odczytanie szczegółowych informacji, w tym statusu, na temat złożonego zamówienia dla wskazanego requisition ID,
- odczytanie informacji na temat złożonych zamówień we wskazanym przedziale czasowym,
- odczytanie szczegółowych informacji na temat podzielonego zamówienia, jeżeli realizacja zamówienia pierwotnego wymagała podziału,
- odczytanie szczegółowych informacji o produktach wysłanych dla wskazanego ID dostawy,
- odczytanie informacji na temat dostaw w siedmiodniowym przedziale czasowym,
- odczytanie szczegółowych danych finansowych klienta, w tym dostępne limity finansowe klienta,
- odczytanie szczegółowych danych adresowych klienta.
