---
sidebar_position: 4
---

# Pricing

Ta operacja pobiera szczegóły cen produktu, takie jak cena katalogowa bez podatku, cena katalogowa z podatkiem, kwota podlegająca zwrotowi, którą musisz zapłacić za produkty podlegające zwrotowi, stawka podatku i cena klienta, która jest obliczana jako cena po wszystkich możliwych zniżkach. Należy zwrócić uwagę, że metoda zwraca cenę pozycji, w której dostępna jest zarówno cena katalogowa, jak i cena klienta. Jednak jako opcja zakupu jest obsługiwana tylko cena klienta.


**Endpoint:** 

`POST /pricing/quote`

Wyślij zapytanie o wycenę (quote), aby uzyskać ceny interesującego Cię przedmiotu. Pola SKU i quantity są wymagane.
Operacja obsługuje do 20 pozycji towarowych jednocześnie.
