---
sidebar_position: 6
---

## Delivery - wyszukanie dostawy

Ta operacja wyszukuje dostawy według różnych parametrów zapytania i pobiera listę dostaw. Obecna wersja obsługuje tylko datę utworzenia dostawy, dzięki czemu można ograniczyć zestaw dostaw do danego okresu. Na przykład można ograniczyć odpowiedź do wybranych dwóch dni (/delivery?creationDateFrom=2021-02-01&creationDateTo=2021-02-02). Dodatkowo, ta metoda obsługuje kontrolę stronicowania (pola limitów i offsetów), które pozwalają kontrolować rozmiar strony zwróconych zamówień. Zauważ, że jeśli nie zdefiniujesz kontroli paginacji, zostanie przyjęta wartość domyślna.

**Endpoint:**

`GET /delivery`

## Pobranie dostawy po identyfikatorze

To operacja pobiera szczegółowe dane dotyczące pojedynczej dostawy. Unikalny identyfikator (deliveryID) jest przekazywany na końcu identyfikatora URI wywołania. Na przykład, jeśli chcesz uzyskać szczegółowe informacje o swojej dostawie PLXYZ00000001, musisz wywołać żądanie jako /delivery/PLXYZ00000001. W odpowiedzi otrzymasz kolekcję dostaw zawierającą wszystkie linie produktów w dostawie.

**Endpoint:**

`GET /delivery/ID`


