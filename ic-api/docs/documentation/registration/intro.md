---
sidebar_position: 1
---

# Wstęp

**Zmiany**

Rejestracja nowego konta odbywa się w pełni przez IC. Klient składa zgłoszenie wysyłając mail na adres [icapi@intercars.eu]( <mailto:icapi@intercars.eu> "icapi@intercars.eu").

W tle zakładane jest konto dostępowe oraz konta w systemach sprzedażowych IC. Po poprawnym skonfigurowaniu i przetestowaniu nowego konta przez IC, klient dostaje informację zwrotną zawierającą:
- adres środowiska produkcyjnego,
- ClientID i ClientSecret dla środowiska testowego SANDBOX,
- ClientID i ClientSecret dla środowiska testowego PROD,
- login i haslo do DevPortal.

Prawidłowy przebieg procesu rejestracji:
1.  Wysłanie maila lub zgłoszenie.
2.  Otrzymanie nowych danych konfiguracyjnych.
3.  Weryfikacja poprawności połączenia i pobierania danych za pomocą narzędzi – Postman, Curl, DevPortal (krok opcjonalny).
4.  Konfiguracja docelowych aplikacji zakupowych/warsztatowych na podstawie instrukcji od poszczególnych dostawców aplikacji sprzedażowych.
5.  Weryfikacja poprawności działania z poziomu aplikacji zakupowych/warsztatowych.

Konfiguracja API  Inter Cars jest bardzo mocno uzależniona od Państwa aplikacji zakupowych/warsztatowych i w tym przypadku zalecamy kontakt z dostawcą oprogramowania.

Poniższy opis przedstawia sposób weryfikacji Państwa nowych ustawień z wykorzystaniem narzędzi ogólnodostępnych. Jest to krok opcjonalny i ostateczna weryfikacja powinna być przeprowadzona z Państwa docelowych aplikacji sprzedażowych.

W razie pytań, nasze Wsparcie Zespołu API jest dostępne pod adresem: [icapi@intercars.eu]( <mailto:icapi@intercars.eu> "icapi@intercars.eu").
