---
sidebar_position: 2
---

# Postman

**Opis przeprowadzenia testu z użyciem programu Postman**

1.  Importujemy pliki z Postman:

    -   [IC.postman_collection.json](../../postman/IC.postman_collection.json)
    -   [PROD IC.postman_environment.json](../../postman/PROD_IC.postman_environment.json)

    Poniżej opis jak zaimportować pliki do programu [postman][Rr]   

    ![Diagram](../../img/image1.png)[🔍Pełny rozmiar w nowej karcie](../../img/image1.png)


**Konfiguracja ustawień API**

1.  Ustawiamy konfigurację na serwery IC API:

    a.  Wybieramy zakładkę Environments

    b.  Następnie PROD IC

    c.  Otwiera się nowa zakładka w oknie głównym

    d.  Po prawej wypełniamy ClientID i ClientSecret (wartości
        przekazane przez IC)

    e.  Adresy serwerów IC wprowadzone są już w pliku konfiguracyjnym w polach baseUrl i tokenUrl (adresy URL środowiska Produkcyjnego i Sandbox są identyczne, rozróżnienie następuje po ClientID i ClientSecret, które są różnne dla obu środowisk)

    f.  Zapisujemy zmiany (Ctrl-S) - po zapisie zniknie czerwona kropka obok napisu PROD IC (pkt 3)

    ![image5.png](../../img/image5.png)

2.  Uwierzytelniamy wywołanie na nowych serwerach IC:

    a.  Wybieramy zakładkę Collections

    b.  Następnie rozwijamy projekt IC i wybieramy operację Authorize

    c.  Pojawia się nowa zakładka (nie musimy nic zmieniać)

    d.  Po prawej wybieramy środowisko **PROD IC**

    e.  Wywołujemy operację Send

    f.  W odpowiedzi w oknie głównym wyświetla się komunikat z
        zawartością tokenu

    ![image6.png](../../img/image6.png)

3.  Wywołujemy operację biznesową:

    a.  W zakładce Collections

    b.  Wybieramy projekt IC i np. operację Inventory (domyślnie
        odpytujemy o towar ADDFFF)

    c.  Wywołujemy operacje Send (warto się upewnić, że wybrane jest
        środowisko **PROD IC**)

    d.  W odpowiedzi dostajemy informację o stanie produktu. Można porównać ją z odpowiedzią uzyskaną podczas odpytania IBM

    e.  W logu możemy podejrzeć szczegóły komunikacji http

    f.  Operację możemy powtórzyć dla dowolnych innych operacji z
        projektu Postman

    ![image7.png](../../img/image7.png)

    [Rr]: <https://learning.postman.com/docs/getting-started/importing-and-exporting-data/>