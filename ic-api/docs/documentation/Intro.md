---
sidebar_position: 1
---

# Wprowadzenie

IC API udostępnia za pomocą publicznych usług REST funkcjonalności pełnej obsługi sprzedaży produktów IC w Polsce i za granicą. IC API (z ang. Application Programming Interface) jest interfejsem umożliwiającym automatyczną wymianę informacji pomiędzy systemami IT klienta (np. sklepem internetowym) a systemami dostawcy IC (magazyny, hurtownie, systemy sprzedażowe). IC API wykorzystuje do komunikacji najpopularniejszy protokół transmisyjny - REST (Representational State Transfer API).  
Innymi słowy, IC API to nic innego jak wtyczka, która pozwala na integrację programu klienta z systemami IC. Dzięki integracji zyskujemy pełną automatyzację wymiany informacji między systemami klienta i IC.
IC REST API:
- Umożliwia automatyzację procesów, które dotychczas realizowane były ręcznie
- Upraszcza wyszukiwanie produktów
- Zapewnia aktualizację stanów produktów
- Pozwala na zaawansowane filtrowanie produktów
- Dostarcza narzędzi analitycznych do optymalizowania
- Umożliwia minimalizację kosztów obsługi i składania zamówień
- Gwarantuje szybkość i niezawodność działania
- Pełne bezpieczeństwo obsługi zamówień

Procesy sprzedażowe IC API dzielimy na główne części:
- Informacje o koncie
- Informacje o produktach
- Składanie zamówień
- Dostawa
- Rozliczenie

Wszystkie operacje API IC są chronione i wymagają rejestracji dedykowanego konta dla klienta. Po stworzeniu konta i uzyskaniu wymaganych dostępów, klient może rozpocząć przygodę z API IC.

Należy też zwrócić uwagę, że API IC jest jednym kanałów sprzedaży IC. Klienci mogą również korzystać z innych narzędzi jak sklep ecommerce czy aplikacje warsztatowe. Wszystkie te kanały umożliwiają wykonanie tych samych operacji.

API IC jest dedykowane dla większych klientów, który chcą mieć możliwość wykonywania bezpośredniej sprzedaży ze swoich sklepów czy też systemów ERP. W tym celu należy przeprowadzić integrację systemów dziedzinowych klienta z API IC.

W pierwszej kolejności klient dostaje dostęp do środowiska testowego, na którym musi przeprowadzić zestaw testów weryfikujących poprawność działania systemu klienta z API IC. Po weryfikacji i akceptacji testów po stronie klienta, jak i IC możemy przejść do pracy na docelowym środowisku produkcyjnym.

Testy mogą być dowolnie rozszerzone przez klienta o inne przypadki biznesowe niezawarte w standardowych testach IC.

REST jest to bezpieczna komunikacja sieciowa. Klient odpytuje API IC, używając protokołu REST i dostaje aktualne dane z systemów dziedzinowych IC.

Funkcjonalność REST API pozwala na pełną integrację i pozyskanie wszystkich potrzebnych informacji. Trzeba jednak zwrócić uwagę, iż nadmierne wykorzystanie API w szczególności do cyklicznej aktualizacji stanów magazynowych może generować znaczny niepotrzebny ruch sieciowy. Wpłynie to negatywnie na wydajność rozwiązania oraz możliwość przekroczenia limitów godzinowej lub dziennej komunikacji. W takim przypadku API IC będzie ograniczało dostęp do wywołań zgodnie z przyjętymi politykami SLA.


Szczegółowy opis funkcjonalności został opisany w kolejnych rozdziałach. Wszystkie operacje dostępne są w swagger oraz projekcie Postman. 
-   [IC.postman_collection.json](../postman/IC.postman_collection.json)
-   [PROD IC.postman_environment.json](../postman/PROD_IC.postman_environment.json)