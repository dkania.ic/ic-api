import React from 'react';
import clsx from 'clsx';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Layout from '@theme/Layout';
import Translate, {translate} from '@docusaurus/Translate';

import styles from './index.module.css';

function HomepageHeader() {
    const {siteConfig} = useDocusaurusContext();
    return (
        <header className={clsx('hero hero--primary', styles.heroBanner)}>
            <div className="container">
                <h1 className="hero__title"><Translate id="siteConfig.title">Documentation</Translate></h1>
                <p className="hero__subtitle">{siteConfig.tagline}</p>
                <div className={styles.buttons}>
                    <Link
                        className="button button--secondary button--lg"
                        to="/docs/documentation/Intro">
                        <Translate
                            id="homepage.start">
                            Start here️
                        </Translate>
                    </Link>
                </div>
            </div>
        </header>
    );
}

export default function Home() {
    const {siteConfig} = useDocusaurusContext();
    return (
        <Layout
            description="Description will go into a meta tag in <head />">
            <HomepageHeader/>
            <main>
                <section className={styles.features}>
                    <div className="container">
                        <div className="row">
                            <div className={clsx('col align-self-center')}>
                                <div className="text--center">
                                    <img
                                        className={styles.featureSvg}
                                        src="img/logo.svg"
                                        alt="bla"
                                    />
                                    <img /><img/>
                                </div>
                                <div className="text--center padding-horiz--md">
                                    <h3><Translate id="homepage.title">Business Documentation</Translate></h3>
                                    <p><Translate id="homepage.description">All you need in one place</Translate></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </Layout>

    );
}

