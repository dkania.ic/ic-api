// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
    title: 'Documentation',
    tagline: 'IC API',
    url: 'https://gitlab.com/intercars/ic-api',
    baseUrl: '/ic-api/',
    onBrokenLinks: 'warn',
    onBrokenMarkdownLinks: 'warn',
    favicon: 'img/favicon.ico',

    // GitHub pages deployment config.
    // If you aren't using GitHub pages, you don't need these.
    organizationName: 'Inter Cars', // Usually your GitHub org/user name.
    projectName: 'IC Docs', // Usually your repo name.

    // Even if you don't use internalization, you can use this field to set useful
    // metadata like html lang. For example, if your site is Chinese, you may want
    // to replace "en" with "zh-Hans".
    i18n: {
        defaultLocale: 'pl',
        locales: ['en','pl'],
    },

    presets: [
        [
            'classic',
            /** @type {import('@docusaurus/preset-classic').Options} */
            ({
                docs: {
                    sidebarPath: require.resolve('./sidebars.js'),
                    // Please change this to your repo.
                    // Remove this to remove the "edit this page" links.
//                    editUrl:
//                        'https://gitlab.com/intercars/ic-api',
                    includeCurrentVersion: true,
                },
                blog: {
                    path: 'news',
                    routeBasePath: 'news',
                    showReadingTime: false,
                    blogTitle: "News",
                    blogDescription: 'Stay in touch'
                },
                theme: {
                    customCss: require.resolve('./src/css/custom.css'),
                },
            }),
        ],
        [
            'redocusaurus',
            {
                // Plugin Options for loading OpenAPI files
                specs: [
                    {
                        spec: 'docs/contracts/api-swagger.yml',
                        route: '/contracts/api',
                    },
//                    {
//                        spec: 'docs/contracts/dropshipping-swagger.yml',
//                        route: '/contracts/dropshipping'
//                    },
//                    {
//                        spec: 'docs/contracts/use-swagger.yml',
//                        route: '/contracts/use'
//                    }
                ],
                // Theme Options for modifying how redoc renders them
                theme: {
                    // Change with your site colors
                    primaryColor: '#1890ff',
                    options: {
                        hideDownloadButton: true,
                    }
                },
            },
        ],
    ],

    themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
        ({
            docs: {
                sidebar: {
                    hideable: true,
                },
            },
            navbar: {
                title: 'IC Docs',
                logo: {
                    alt: 'Logo strony',
                    src: 'img/logo.svg',
                },
                items: [
//                    {to: '/docs/introduction/Intro', label: 'Wstęp', position: 'left'},
                    {to: '/docs/documentation/Intro', label: 'Documentation', position: 'left'},
                    {
                        label: 'Swagger',
                        type: 'dropdown',
                        position: 'left',
                        items: [
                            {
                                label: "IC API",
                                href: '/contracts/api'
                            },
//                            {
//                                label: "Dropshipping API",
//                                href: '/contracts/dropshipping'
//                            },
//                            {
//                                label: "USE API",
//                                href: '/contracts/use'
//                            }
                        ]
                    },
//                    {to: '/news', label: 'Wiadomości', position: 'left'},
//                    {to: '/changelog', label: 'Changelog', position: 'left'},
                    {
                        type: 'localeDropdown',
                        position: 'right',
                    },
//                    {
//                        href: 'https://gitlab.com/intercars/ic-api',
//                        label: 'GitLab',
//                        position: 'right',
//                    },
                ],
            },
            footer: {
                style: 'dark',
                links: [
                    {
                        title: 'Contact us',
                        items: [
                            {
                                label: 'FAQ',
                                to: '/docs/contact/faq',
                            },
                            {
                                label: 'Submit issue',
                                to: '/docs/contact/submit-issue',
                            }
                        ],
                    },
                ],
                copyright: `Copyright © ${new Date().getFullYear()} IC S.A.`,
            },
            prism: {
                theme: lightCodeTheme,
                darkTheme: darkCodeTheme,
            },
            tableOfContents: {
                minHeadingLevel: 2,
                maxHeadingLevel: 5,
            },
        }),
    plugins: [
        [
            require.resolve('./src/plugins/changelog/index.js'),
            {
                blogTitle: 'IC Changelog',
                blogDescription:
                    'Changelog',
                blogSidebarCount: 'ALL',
                blogSidebarTitle: 'Changelog',
                routeBasePath: '/changelog',
                showReadingTime: false,
                postsPerPage: 20,
                archiveBasePath: null,
                authorsMapPath: 'authors.json',
                feedOptions: {
                    type: 'all',
                    title: 'Docusaurus changelog',
                    copyright: `Copyright © ${new Date().getFullYear()} IC S.A. `,
                    language: 'en',
                },
            },
        ],]
};

module.exports = config;
