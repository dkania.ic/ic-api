1. How to contact IC

`ToDo`

2. How to activate a new client

3. How to sign the contract
4. How to solve the technical problem
5. What is Dropshipping?
6. Who is Dropshipping for?
7. Returns and complaints about goods
8. Are the customer ID and customer ID the same number?

`NO. Customer ID is a six-character identifier assigned to your company in the Inter Cars ERP system. The client ID is a much longer internal identifier in the API and is matched in our database with your client ID. Every time you change your ClientID to API - and you can do it at any time - you need to send this number to icapi@intercars.eu to have the mapping update to the ERP system.`

9. What is "sku" and how do I get it?

`sku' is an API item identifier, unique in the entire Inter Cars catalogue. Every component, every bolt, tire, wiper, etc. can be identified and ordered by this value. It is alternatively called TOWKOD and can be found, for example, in CSV files generated manually or on a schedule. The corresponding column in the CSV file is "ID".`

10. What method of payment or method of transport should I enter? I get an error every time.

The method of payment and transport is assigned to your account in the ERP system and is downloaded from there. forcing to override these settings may cause errors. To ensure proper order fulfillment, do not enter these values or parameters at all. Your account was tested for this before it was transferred as owner.`

11. For some products I get an error in the API even though I know they are in stock.

`This is due to the fact that Sandbox is a test environment and does not include all items and storages set up on a production environment. There is only a partial representation to make the test environment run quickly and efficiently during testing. You'll get replies from major HUBs instead of your home magazine

12. I place an order but I don't see an invoice or delivery.

`Sandbox is a test environment and does not simulate the entire order cycle. The moment you will arrive by default is placing an order and it remains in status "A" pending manual confirmation. Thus, it does not move to the "completed" or "invoiced" status. However, you can try to search your real (production environment) invoices or deliveries as Sandbox offers historical data for your own purchases.`

13. Errors appear when searching by date.

The `API allows you to search your entire purchase history, but only in a very limited window. For example, you can search for an invoice from 8 months ago, but only if you specify a 7-day timeframe. So, to search the whole month, you would have to ask 4 times, asking for another week each time. Delivery notes have a window frame for 2 days, but can also be viewed in their entire Purchase history. `

14. How to import a list of goods from a CSV file to an API order?

`API does not support import from CSV files.`

15. Who should I call if I have a question?

`First, always try to find the answer in the swagger file or definitions in the API / API PRODUCTS tab. If you still have problems you can email icapi@intercars.eu with the name of the app (from the APPLICATIONS tab) and exactly what you want to do and the steps you take including screenshots, links and lines code and error messages if possible. As the API is a living project they are constantly being improved and developed, there may be some changes when using the API that have not yet been included in the documentation you now have. Technical problems are not resolved over the phone.`