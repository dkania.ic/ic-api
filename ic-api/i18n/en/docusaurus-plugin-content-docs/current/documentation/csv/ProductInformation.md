---
sidebar_position: 2
---

# ProductInformation

A list of products containing descriptions about the product
- Files are available in the ProductInforma directory
- Files are generated once a day around 04:00-05:30 AM Polish time
- The files contain data grouped in the following columns

| No. | Field name        | Data type | Description                                  |
|-----|-------------------|-----------|----------------------------------------------|
| 1.  | TOW_KOD           | TEXT      | Unique product identifier (SAFO towcode)     |
| 2.  | IC_INDEX          | TEXT      | Unique Product Identifier (SAFO Index)       |
| 3.  | TEC_DOC TEXT      | TEXT      | Product ID from Tec Doc (ArtNr)              |
| 4.  | TEC_DOC_PROD      | NUMBER    | Manufacturer ID from Tec Doc                 |
| 5.  | ARTICLE_NUMBER    | TEXT      | Identifier from the SAFO parts catalog       |
| 6.  | MANUFACTURER      | TEXT      | Manufacturer's name from SAFO                |
| 7.  | SHORT_DESCRIPTION | TEXT      | A short description of the product from SAFO |
| 8.  | DESCRIPTION       | TEXT      | Description extended from SAFO               |
| 9.  | BARCODES          | TEXT      | EAN codes                                    |
| 10. | PACKAGE_WEIGHT    | NUMBER    | The weight of the product                    |
| 11. | PACKAGE_LENGTH    | NUMBER    | Length of the product                        |
| 12. | PACKAGE_WIDTH     | NUMBER    | Width of the product                         |
| 13. | PACKAGE_HEIGHT    | NUMBER    | Product height                               |
| 14. | CUSTOM_CODE       | NUMBER    | Customs code                                 |


![dropshippingImage1](../../img/productInformation.png)