---
sidebar_position: 1
---
# Introduction

CSV files can be shared in three ways:
- Under the HTTP resource
- On the FTP server of the IC
- Placed directly on the client's FTP server.

CSV files contain large amounts of items. The recommended communication for the client is to download the CSV from the HTTP resource.

Product catalog functionality may replace CSV downloads in the future.


# Sharing
Files are provided in CSV format, where a semicolon is used as a separator. The first line of the file contains column names - headers in the meaning of RFC 4180 para. 2.3. The text values in the fields may be enclosed in quotation marks, e.g.: "B12 Pro-Kit Sport Suspension Kit (876/943 kg;25/20mm) Subaru BRZ 2.0 06.12-".

Each client sees personalized data in CSV files. CSV files can be downloaded by the customer from the website <https://data.webapi.intercars.eu/customer> after receiving the access password.

We only provide CSV files to the recipient.

# Categories
Files are available in five directories:

- ProductInformation - contains information about the product,
- WholesalePricing - contains information on net prices,
- Stock - contains information about the availability of products in the location,
- Stock_Price – contains information about prices and availability of a given stock,
- Pictures - contains links to pictures

# Security
The above-mentioned data are made available from the server using the HTTPS protocol. Authentication takes place using BasicAuth mechanisms in accordance with RFC 7617 and RFC 7235 documents. Files can be downloaded by end users manually using a web browser or other tool communicating using the HTTP protocol and automatically using a built-in integration with our system promoting the above. data - using the same protocol.
For manual editing of the downloaded file, it is recommended to use the Notepad ++ software, available for download at Notepad++. When using Microsoft Excel, keep in mind its limited capacity of just over 1 million lines. The use of MS Excel versions below 2007 is not recommended.