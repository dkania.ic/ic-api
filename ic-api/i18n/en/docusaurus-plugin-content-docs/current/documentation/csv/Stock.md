---
sidebar_position: 4
---

# Stock

A list of products containing information about stock levels:
- These files are available in the Stock directory
- Files are generated around 03:00 - 04:20 AM Polish time
- The files contain data grouped in the following columns


| No. | Field name         | Data type | Description                              |
|-----|--------------------|-----------|------------------------------------------|
| 1.  | TOW_KOD            | TEXT      | Unique product identifier (SAFO towcode) |
| 2.  | IC_INDEX           | TEXT      | Unique Product Identifier (SAFO Index)   |
| 3.  | TEC_DOC TEXT       | TEXT      | Product ID from Tec Doc (ArtNr)          |
| 4.  | TEC_DOC_PROD       | NUMBER    | Manufacturer ID from Tec Doc             |
| 5.  | WAREHOUSE          | TEXT      | Warehouse or Branch                      |
| 6.  | AVAILABILITYNUMBER | NUMBER    | The number of products available         |