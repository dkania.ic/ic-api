---
sidebar_position: 3
---

# WholesalePricing

List of products containing information about net prices for the customer:
- These files are available in the WholesalePricing directory
- Files are generated around 04:30 - 05:30 AM Polish time
- The files contain data grouped in the following columns

| No. | Field name          | Data type | Description                                                |
|-----|---------------------|-----------|------------------------------------------------------------|
| 1.  | TOW_KOD             | TEXT      | Unique product identifier (SAFO towcode)                   |
| 2.  | IC_INDEX            | TEXT      | Unique Product Identifier (SAFO Index)                     |
| 3.  | TEC_DOC             | TEXT      | Tec Doc Product Identifier (ArtNr)                     |
| 4.  | TEC_DOC_PROD        | NUMBER    | Manufacturer ID from Tec Doc                               |
| 5.  | WHOLESALE_NET_PRICE | NUMBER    | Wholesale price                                            |
| 6.  | CORE_PRICE          | NUMBER    | The price of the part subject to regeneration              |
| 7.  | SUM_PRICE           | NUMBER    | The sum of the wholesale price and the remanufactured part |