---
sidebar_position: 5
---

# StockPrice

Summary of products containing information about stock levels and prices for the customer:
- These files are available in the Stock_price directory
- Files are generated usually 03:00 - 04:20 AM Polish time
- The files contain data grouped in the following columns

| No. | Field name       | Data type | Description                                                |
|-----|------------------|-----------|------------------------------------------------------------|
| 1.  | TOW_KOD          | TEXT      | Unique product identifier (SAFO towcode)                   |
| 2.  | WAREHOUSE        | TEXT      | Warehouse or Branch                                        |
| 3.  | AVAILABILITY     | NUMBER    | The number of products available                           |
| 4.  | WHOLESALE_PRICE  | NUMBER    | Wholesale price                                            |
| 5.  | CORE_PRICE       | NUMBER    | Price of the part subject to regeneration                  |
| 6.  | SUM_PRICE NUMBER | NUMBER    | The sum of the wholesale price and the remanufactured part |
| 7.  | RETAIL_PRICE     | NUMBER    | Net retail price                                           |