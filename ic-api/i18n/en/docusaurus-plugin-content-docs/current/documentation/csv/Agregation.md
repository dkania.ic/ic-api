# Data aggregations

Additional aggregations allow you to filter data according to the customer's request:

1. File generation language > PIC
2. Available segments > PIC
3. Available markets
4. Currency as configured in ERP or NAV
5. Aggregation by locations
6. File schedules when they are to be generated:
    - Daily Schedule A
    - Once a week schedule B1 Monday B2 Tuesday …….
    - Once a month C+day of the month (e.g. 1 every first day of the month)
    - remember that the ProductInformation and WholesalePricing files must have this set using
      same schedule
    - in the Stock file you can set a schedule to refresh several times a day (every 2h - 3h)
7. Pictures of the goods
8. Visibility of the core index in the file or not:
    - no core index visibility
    - with core index visibility
9. Tree leaf aggregation:
    - that is, if the customer wants only pads, tires, rims, etc..
10. Aggregation by goods status
11. Aggregation by TecDoc:
    - set to true/false depending on whether the customer wants only products that have a TecDoc number or the entire offer
12. Ability to set the file to be compressed (*.zip) or not
13. Possibility to choose the file generation name (suffix with the date added every day or fixed file name).
    Example:
    The default filename is created as follows:
    <File_type_year_month_day>.csv, e.g. Stock_2022-05-30.csv, after compression the file will be named Stock_2022-05-30.csv.zip
14. Possibility to overwrite the file, i.e. the file will be in Stock.csv format and Stock.csv.zip after compression