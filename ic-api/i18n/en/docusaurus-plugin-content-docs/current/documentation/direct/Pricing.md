---
sidebar_position: 4
---

#Pricing

This operation retrieves the details of a specific product price, such as list price without tax, list price with tax, refundable amount you have to pay for refundable products, tax rate and customer price which is calculated as the price after all possible discounts. Note that the method returns the price of an item where both the list price and the customer price are available. However, only the customer price is supported as a purchase option.

The service is dedicated to retrieving the contractor's price list.
For the client, the service returns the prices of the defined type calculated for preset number.
Use of this service depends on requirements of the integration.

**endpoint:**

`GET /dropshipping/pricing/quote`

Send a PRICING QUOTE request to get prices of the item you are interested in. The SKU and quantity fields are
required.