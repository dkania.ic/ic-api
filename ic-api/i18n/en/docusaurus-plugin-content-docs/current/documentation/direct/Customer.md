---
sidebar_position: 2
---

## Customer finances

This operation returns the customer's financial details.

The operation is dedicated to checking whether it is possible to place orders in the online store.

Returns a set of data from the ERP system, such as:
- available limits
- available credit
- ordering allowed

The service must be used at least once a day and the Dropshipping Operator must regularly check the response before starting to send orders.
The scope of data returned by the service is sufficient to take actions reducing the risk of blocking the execution of orders by the IC.

**endpoint:**

`GET /customer/finances`

## Customer settings

This operation returns customer details

**endpoint:**

`GET /customer`