---
sidebar_position: 5
---

# Sales

## Order search

This operation searches for orders related to your requirements by various query parameters and retrieves order summaries. The current version only supports the creation date of the requisition, so you can limit the set of orders to a certain period. For example, you can limit the response to the last two days (/sales/requisition? CreationDateFrom=2018-01-09 and creationDateTo=2018-01-11). In addition, this method supports paging controls (limit and offset fields) that allow you to control the page size of returned orders. Note that if you don't define a pagination control, a default value will be assumed.

**endpoint:**

`GET /sales/requisition`

## Download order by ID

This operation retrieves confirmed and unconfirmed orders related to a specific demand. The unique identifier (requisition.id) is passed at the end of the call URI. For example, if you want to get the details of the demand IC10000001, you need to trigger the request as [URL]/sales/request/IC10000001. In response, you will receive a collection of orders that will show the current status of implementation.

**endpoint:**

`GET /sales/requisition/ID`

## Retrieving order details by ID

This operation retrieves details about a single order from your request. The unique identifier (order.id) is passed at the end of the call URI. For example, if you want to get the details of order 1100130601, you need to call the request as [URL]/sales/order/1100130601. In response you will get the details of a single order from your order. This method is useful in the invoicing and shipping process and allows you to see which order these documents were prepared for. "

**endpoint:**

`GET /sales/order/ID`

## Order

This operation is used to create a requirement for a specific inventory item. It's up to you whether you want to create a complete order from the start, or you can fill in the information the first time the purchase requisition is triggered and then do one of the following - confirm or cancel the order. Information that will be required before order is posted - item SKU, requisition price and required quantity. Optionally, you can provide information to narrow down your demand, such as - delivery method, payment method. Please note that if you do not fill in this information, the requisition will be saved with your own preferences from the customer IC card. In addition, you can save your own number and decide that the request will be submitted for deferred payment. If the connection is successful, orders with a unique requisition ID are returned in response. This ID will be required for other demand connections. “All detailed information about individual fields has been described in the Demand and order object.

**endpoint:**

`POST /sales/requisition`