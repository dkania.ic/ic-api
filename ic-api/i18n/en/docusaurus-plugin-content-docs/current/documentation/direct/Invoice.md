---
sidebar_position: 7
---

## Invoice search

This operation retrieves the details of a single invoice. The unique identifier (invoice.id) is passed at the end of the call URI. For example, if you want to get details about your invoice PL00000001, you need to call the request as /invoice/PL00000001. In response, you will receive invoice details with references to orders and deliveries, allowing you to match the document to your orders/requisitions.

**endpoint:**

`GET /invoice`

## Fetch invoices by ID

This operation retrieves the details of a single delivery. The unique identifier (delivery.id) is passed at the end of the call URI. For example, if you want details about your PLXYZ00000001 delivery, you need to call the request as /delivery/PLXYZ00000001. In response, you will receive a delivery collection containing all product lines in the delivery.

**endpoint:**

`GET /invoice/ID`