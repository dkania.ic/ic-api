---
sidebar_position: 3
---

# Inventory - Stock

This operation retrieves details of the estimated number of products available for purchase. The calling system through the service may retrieve data on the availability of certain stock items, for example, when handling sales orders. If the location of the Inter Cars warehouse is specified in the operation call, the status is only for this warehouse. Otherwise, the status is returned for all warehouses in the customer's logistics path

The service Dropshipping/Stock is designed to determine the availability of a list of products according to input parameters.
The following parameters should be specified in the input parameters:
- default warehouse and / or defined logistic path
- number of item units expected
- SKU identifier

As a result, the service returns a number greater than or equal to '0' (zero) for each article. It handles a list of 30 items at a time.
The service does not interpret the result - the logic should be implemented on the side of the dropshipping platform.
Send a stock inquiry using GET INVENTORY/STOCK, by placing a sku (TOWKOD / INDEX / ID field from InterCars CSV
files)

**endpoint:**

`POST /inventory/stock`