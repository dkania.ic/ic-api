---
sidebar_position: 6
---

## Delivery search

This operation searches for deliveries by various query parameters and fetches a list of deliveries. The current version only supports delivery creation date, so you can limit the delivery set to a certain period. For example, you can limit the response to the last two days (/Delivery? CreationDateFrom = 2018-01-09 and CreationDateTo = 2018-01-11). In addition, this method supports pagination controls (limit and offset fields) that allow you to control the page size of returned orders. Note that if you don't define a pagination control, a default value will be assumed.

**endpoint:**

`GET /delivery`

## Retrieving delivery by ID

This operation retrieves the details of a single delivery. The unique identifier (delivery.id) is passed at the end of the call URI. For example, if you want details about your PLXYZ00000001 delivery, you need to call the request as /delivery/PLXYZ00000001. In response, you will receive a delivery collection containing all product lines in the delivery.

**endpoint:**

`GET /delivery/ID`