---
sidebar_position: 1
---

# Introduction

The IC API Direct model allows you to sell goods and deliver them to the partner's warehouse. Among the operations and functionalities that support buying/selling you can find:

- checking the availability of products in the basket (one request for a basket up to 100 items), with the option of indicating warehouses from the logistics path,
- downloading the current prices of products in the basket (one request for a basket of up to 20 items),
- placing an order with current prices,
- adding your own order number,
- adding a comment to the order,
- control of configurable payer recipients within orders and stock reps (Master special accounts),
- receiving a requisition ID,
- reading detailed information about the placed order for the indicated requisition ID,
- reading information on orders placed in the indicated time period,
- reading detailed information on the split order, if the execution of the original order required splitting,
- reading detailed information about the products sent for the indicated delivery ID,
- reading information on deliveries in the indicated time period up to 7 days back,
- reading the client's detailed financial data - client's limit,
- reading detailed customer address data.