---
sidebar_position: 1
---
# Introduction

IC API provides, via public REST services, the functionalities of full support for the sale of IC products in Poland and abroad. The IC API (Application Programming Interface) is an interface that enables the automatic exchange of information between the client's IT systems (e.g. an online store) and the IC supplier's systems (warehouses, warehouses, sales systems). IC API uses the most popular transmission protocol - REST (Representational State Transfer API) for communication.

In other words, the IC API is nothing more than a plug-in that allows you to integrate the client program with IC systems. Thanks to the integration, we gain full automation of information exchange between the client's and IC systems.

IC REST API:
- Enables the automation of processes that were previously carried out manually
- Simplifies product search
- Provides product status updates
- Allows for advanced filtering of products
- Provides analytical tools for optimization
- Allows you to minimize the costs of handling and placing orders
- Guarantees speed and reliability of operation
- Full security of order processing

IC API sales processes are divided into main parts:
- Account information
- Product information
- Placing orders
- Delivery
- Billing

All API IC operations are protected and require registration of a dedicated account for the client. After creating an account and obtaining the required access, the client can start his adventure with the IC API.

It should also be noted that the IC API is one of the IC sales channels. Customers can also use other tools, such as an e-commerce store or workshop applications. All these channels allow you to perform the same operations.

API IC is dedicated to larger customers who want to be able to make direct sales from their stores or ERP systems. For this purpose, the client's domain systems must be integrated with the API IC.

First, the client gets access to the test environment on which he must run a set of tests verifying the correct operation of the client's system with API IC. After verification and acceptance of tests on the client and IC side, we can go to work on the target production environment.

The tests can be freely extended by the client with other business cases not included in the standard IC tests.

REST is secure network communication. The client queries the IC API using the REST protocol and gets current data from the IC domain systems.

The REST API functionality allows for full integration and obtaining all the necessary information. However, it should be noted that excessive use of the API, in particular for cyclical updating of stock levels, may generate significant unnecessary network traffic. This will negatively affect the performance of the solution and the possibility of exceeding hourly or daily communication limits. In this case, API IC will limit access to calls in accordance with the adopted SLA policies.

A detailed description of the functionality is described in the following chapters. All operations are available in ger and postman project: 
-   [IC.postman_collection.json](../postman/IC.postman_collection.json)
-   [PROD IC.postman_environment.json](../postman/PROD_IC.postman_environment.json)