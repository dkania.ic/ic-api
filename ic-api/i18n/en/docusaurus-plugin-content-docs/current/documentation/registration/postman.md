---
sidebar_position: 2
---

# Postman

**Description of how to conduct a test using Postman**

1.	We need to import files from Postman:

    -   [IC.postman_collection.json](../../postman/IC.postman_collection.json)
    -   [PROD IC.postman_environment.json](../../postman/PROD_IC.postman_environment.json)

    Below is a description of how to import files into the [postman][Rr]   


**API new settings**

2. We rewrite the configuration for the new IC API servers:

    a.	Let’s select the Environments tab

    b.	Then PROD IC

    c.	A new tab opens in the main window

    d.	On the right, fill in ClientID and ClientSecret (new values provided by IC)

    e.	The new IC server addresses are already entered in the configuration file in the baseUrl and tokenUrl fields

    f.	Let’s save the changes (Ctrl-S) - after saving, the red dot next to the word PROD IC (dot 3) will disappear

    ![image5.png](../../img/image5.png)[🔍Full size in new tab](../../img/image5.png)

3. Next we authenticate the call on the new IC servers:

    a.	We select the Collections tab

    b.	Then we expand the IC project and select Authorize operation

    c.	A new tab appears (we do not need to change anything)

    d.	On the right, we select the PROD IC environment

    e.	We click the Send button
    
    f.	In response, the main window displays a message with the contents of the token

    ![image6.png](../../img/image6.png)[🔍Full size in new tab](../../img/image6.png)

4.  We make the call including business operation:

    a.	On the Collections tab

    b.	We select the IC project and, for example, the Inventory operation (by default we query the ADDFFF product)

    c.	We click the Send button (it is worth making sure that PROD IC environment is selected)

    d.	In response we get information about the status of the product

    e.	You can compare it with the response you got when you queried IBM

    f.	In the log we can preview the details of the http communication

    g.	We can repeat the operation for any other operations from the Postman project

    ![image7.png](../../img/image7.png)[🔍Full size in new tab](../../img/image7.png)

[Rr]: <https://learning.postman.com/docs/getting-started/importing-and-exporting-data/>