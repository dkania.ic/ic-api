---
sidebar_position: 1
---

# Intro

Registration proccess of a new account is done entirely by IC. The customer submits the application by sending an e-mail to [icapi@intercars.eu]( <mailto:icapi@intercars.eu> "icapi@intercars.eu").

In the background, API access account and accounts in IC sales systems are set up. After the new account is properly configured and tested by the IC, the client receives feedback containing:

- address of the production environment,
- ClientID and ClientSecret for SANDBOX test environment,
- ClientID and ClientSecret for PROD test environment,
- login and password for DevPortal.
  
The correct steps of the migration process:

- Receiving new configuration data,
- Verification of the connection correctness and data download using tools - Postman, Curl, DevPortal (optional step),
- Reconfiguration of target purchasing/workshop applications based on instructions from individual vendors of sales applications,
- Verification of correct operation from the level of purchasing/workshop applications.


Reconfiguration of the Inter Cars API is very much dependent on your purchasing applications and in this case we recommend contacting the software supplier.<br />

The description below shows how to verify your new settings using publicly available tools.
This is an optional step and target verification should be performed from your target sales apps

If you have any questions, our API Team Support is available at: [icapi@intercars.eu]( <mailto:icapi@intercars.eu> "icapi@intercars.eu").