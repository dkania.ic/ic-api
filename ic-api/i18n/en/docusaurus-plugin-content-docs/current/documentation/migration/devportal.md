---
sidebar_position: 4
---

# DevPortal

Description of performing a test using the DevPortal application

1.	Go to the website 
    <https://cp.webapi.intercars.eu/devportal/apis>

2.	In the login window  
    a.	enter login and password:

    ![image3-1.png](../../img/image3-1.png)

    b.	Please do not select Create an account - this functionality is temporarily unavailable

    c.	Please do not select Password Reminder - this functionality is temporarily unavailable - if you forget your password, please contact the IC to generate a new password


3.	After logging in, the home screen is displayed with a list of available applications

    a.	1 > API tab allowing you to perform basic operations with the API

    b.	2 > Applications tab allowing you to generate API grouping applications

    c.	3 > Logged-in user information 

    d.	4 > Main screen displaying details of the selected option 1 or 2

    ![image3-2.png](../../img/image3-2.png)

4.  Go to the details by selecting API 1 and IC 4 (depending on the permissions, the logged-in user can see more APIs, e.g. IC, Dropshiping, Product). After selecting the details, we get a screen with:

    
    a.	General informations
    
    b.	Subscriptions - ability to view keys
    
    c.	Try Out - ability to test the application 
    
    d.	Comments - ability to comment
    
    e.	Documentation - a detailed description will appear in future versions.

    ![image3-3.png](../../img/image3-3.png)

5.	Subscriptions tab

    a.	Allows for the selected application to view Key and Secret for Sandbox and Production environments - DevPortal console values for the selected environments should be identical to the ClientID and ClientSecret provided by the IC.

    b.	Generate Access Token button allows you to generate a token to be used in testing

    ![image3-4.png](../../img/image3-4.png)

6.	Try Out tab Allows you to test the service

    a.	Select the environment

    b.	Generate a Token - we can preview it here

    ![image3-5.png](../../img/image3-5.png)

    c.	Select the operation we want to test, read the documentation 

    ![image3-6.png](../../img/image3-6.png)

    d.	Select the "Try it out", specify the parameters and run "Execute"

    ![image3-7.png](../../img/image3-7.png)

    e.	We can see the exact request sent to the API and the server's response.

    ![image3-8.png](../../img/image3-8.png)

    f.	From the Try it out tab we can call any operation on the IC API, including placing an order. Performing such an operation is equivalent to placing an order which will be immediately processed by the IC warehouse.

 
7.	The Applications tab allows you to check the plans assigned to the application and security settings similarly to the option above.

    ![image3-9.png](../../img/image3-9.png)

8.	After selecting the application details are displayed where we can

    a.	View the settings for the Production environment

    b.	View the settings for the Sandbox environment

    c.	View the settings for subscribed APIs

    ![image3-10.png](../../img/image3-10.png)

**CAUTION !!!**
Please do not change the settings for Application and Subscription. Adding a new configuration will not be supported by IC. Please base on the ones that are created and treat this data only for preview.


9.	We can also change the generated password to our own by selecting the user login and the option "Change Password".

    ![image3-11.png](../../img/image3-11.png)
