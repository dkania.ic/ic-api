---
sidebar_position: 2
---

# Postman

**Description of how to conduct a test using Postman**

1.	We need to import files from Postman

    -   [IC.postman_collection.json](../../postman/IC.postman_collection.json)
    -   [PROD IC.postman_environment.json](../../postman/PROD_IC.postman_environment.json)
    -   [PROD IBM.postman_environment.json](../../postman/PROD_IBM.postman_environment.json)

    Below is a description of how to import files into the [postman][Rr]   

    ![Diagram](../../img/image1.png)[🔍Full size in new tab](../../img/image1.png)

2. We need to configure the settings for IBM

    a.	Select the Environments tab

    b.	Then PROD IBM

    c.	On the right, fill in ClientID and ClientSecret

    d.	Save the changes (Ctrl-S) - after saving, the red dot next to PROD IBM should disappear

    ![image2.png](../../img/image2.png)[🔍Full size in new tab](../../img/image2.png)

3. We authenticate the call to IBM using steps below:

    a.	We should select the Collections tab

    b.	Then we expand the IC project and select Authorize operation

    c.	A new tab appears (we do not need to change anything)

    d.	On the right, we select the PROD IBM environment 

    e.	We click the Send button

    f.	In response, the main window displays a message with the contents of the token.

        The token will be used in subsequent calls

    g.	At the bottom in the Console tab, we can see the exact content of the request that Postman has generated and sent

    ![image3.png](../../img/image3.png)[🔍Full size in new tab](../../img/image3.png)

4. Next we make the call including business operation

    a.	On the Collections tab

    b.	We select the IC project and, for example, the Inventory operation (by default we query the ADDFFF product)

    c.	Invoke the Send operation (it is a good idea to make sure that the PROD IBM environment is selected)

    d.	In response we get information about the status of the product

    e.	In the log we can view the details of the HTTP communication

    ![image4.png](../../img/image4.png)[🔍Full size in new tab](../../img/image4.png)

    **API works correctly on old settings**

5. We rewrite the configuration for the new IC API servers
    
    a.	Let’s select the Environments tab

    b.	Then PROD IC

    c.	A new tab opens in the main window

    d.	On the right, fill in ClientID and ClientSecret (new values provided by IC)

    e.	The new IC server addresses are already entered in the configuration file in the baseUrl and tokenUrl fields

    f.	Let’s save the changes (Ctrl-S) - after saving, the red dot next to the word PROD IC (dot 3) will disappear

    ![image5.png](../../img/image5.png)[🔍Full size in new tab](../../img/image5.png)

6. Next we authenticate the call on the new IC servers 

    a.	We select the Collections tab

    b.	Then we expand the IC project and select Authorize operation

    c.	A new tab appears (we do not need to change anything)

    d.	On the right, we select the PROD IC environment

    e.	We click the Send button

    f.	In response, the main window displays a message with the contents of the token

    ![image6.png](../../img/image6.png)[🔍Full size in new tab](../../img/image6.png)

7. We make the call including business operation

    a.	On the Collections tab

    b.	We select the IC project and, for example, the Inventory operation (by default we query the ADDFFF product)

    c.	We click the Send button (it is worth making sure that PROD IC environment is selected)

    d.	In response we get information about the status of the product

    e.	You can compare it with the response you got when you queried IBM

    f.	In the log we can preview the details of the http communication

    g.	We can repeat the operation for any other operations from the Postman project

    ![image7.png](../../img/image7.png)[🔍Full size in new tab](../../img/image7.png)

[Rr]: <https://learning.postman.com/docs/getting-started/importing-and-exporting-data/>