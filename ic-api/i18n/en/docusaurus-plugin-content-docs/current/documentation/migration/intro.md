---
sidebar_position: 1
---

# Intro

**Changes**

Due to the change of Inter Cars API servers, there is a need to reconfigure the connection of your purchasing software.

This change is aimed at optimizing the Inter Cars API tool and thus increasing its efficiency and speed.

Modifications on the client side are very simple to carry out. 

**From the viewpoint of:**

- development:
  - **all interfaces (services, operations, parameters) <u>will not change</u>**
- administration:
   - **The URL of the API server will change**
   - **ClientId and ClientSecret will change (for Prod and Sandbox environment)**
   - **The URL, login, password and functionality of the DevPortal also change.**

All old accounts will be registered automatically in DevPortal by the Inter Cars API Team. New URLs and ClientIDs and ClientSecrets for all Clients will be communicated individually.

The correct steps of the migration process:
- Receiving new configuration data
- Verification of the correctness of the connection and data download using tools - Postman, Curl, DevPortal (optional step)
- Reconfiguration of target purchasing/workshop applications based on instructions from individual vendors of sales applications
- Verification of correct operation from the level of purchasing/workshop applications.


Reconfiguration of the Inter Cars API is very much dependent on your purchasing applications and in this case we recommend contacting the software supplier.<br />

The description below shows how to verify your new settings using publicly available tools.
This is an optional step and final verification should be performed from your target sales apps.

If you have any questions, our API Team Support is available at: [icapi@intercars.eu]( <mailto:icapi@intercars.eu> "icapi@intercars.eu").
